<?php

 /*

 penulis: Muhammad yusuf
 website: http://www.kodingindonesia.com/

 */
 	// $satgas = $_GET['id'];

	//Import File Koneksi Database
	require_once('koneksi.php');

	//Membuat SQL Query
	$sql = " SELECT c.nama, c.nik, c.dom_kec, e.nama AS kecamatan, c.dom_kel, f.nama AS kelurahan, c.alamat_domisili, c.no_hp, d.singkatan AS status_covid, a.tgl_kontrol
FROM
(SELECT id_pendaftaran, status_pasien, tgl_kontrol
FROM hasil_rawat
WHERE tgl_kontrol IS NOT NULL) a
LEFT JOIN daftar_rawat b ON a.id_pendaftaran=b.id
LEFT JOIN pasien c ON b.id_pasien=c.id
LEFT JOIN master_status_pasien d ON a.status_pasien=d.id
LEFT JOIN master_kecamatan e ON c.dom_kec=e.id
LEFT JOIN master_kelurahan f ON c.dom_kel=f.id
WHERE a.tgl_kontrol >= DATE(NOW())  ";
	
	//Mendapatkan Hasil
	$r = mysqli_query($connect,$sql);

	//Membuat Array Kosong
	$result = array();

	while($row = mysqli_fetch_array($r)){

		//Memasukkan Nama dan ID kedalam Array Kosong yang telah dibuat
		array_push($result,array(
			"nik"=>$row['nik'],
			"nama"=>$row['nama'],
			"no_hp"=>$row['no_hp'],
			"dom_kec"=>$row['dom_kec'],
			"kecamatan"=>$row['kecamatan'],
			"dom_kel"=>$row['dom_kel'],
			"kelurahan"=>$row['kelurahan'],
			"alamat_domisili"=>$row['alamat_domisili'],
			"tgl_kontrol"=>$row['tgl_kontrol'],
			"status_covid"=>$row['status_covid']
		));
	}


	//Menampilkan Array dalam Format JSON
	echo json_encode($result);

	mysqli_close($connect);
?>
