<?php

 /*

 penulis: Muhammad yusuf
 website: http://www.kodingindonesia.com/

 */
 	// $satgas = $_GET['id'];

	//Import File Koneksi Database
	require_once('koneksi.php');

	//Membuat SQL Query
	$sql = " SELECT * FROM activity_reminder  ";
	
	//Mendapatkan Hasil
	$r = mysqli_query($connect,$sql);

	//Membuat Array Kosong
	$result = array();

	while($row = mysqli_fetch_array($r)){

		//Memasukkan Nama dan ID kedalam Array Kosong yang telah dibuat
		array_push($result,array(
			"id_reminder"=>$row['id_reminder'],
			"aktivitas_reminder"=>$row['aktivitas_reminder'],
			"hasil_reminder"=>$row['hasil_reminder'],
			"status_kooperatif"=>$row['status_kooperatif'],
			"created_at"=>$row['created_at'],
			"created_by"=>$row['created_by']
		));
	}


	//Menampilkan Array dalam Format JSON
	echo json_encode($result);

	mysqli_close($connect);
?>
