package com.agsatu.mobilesatgas.screening

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.hardware.Camera
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.main.MainActivity
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.MediaHelper
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.frag_skrining_face_id.view.*
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.text.DecimalFormat

@Suppress("DEPRECATION")
class FragScreeningFaceId : Fragment(), View.OnClickListener {
    lateinit var thisParent : MainActivity
    lateinit var v : View
    var fileUri : Uri = Uri.parse("")
    var imStr = ""
    lateinit var mediaHelper: MediaHelper
    lateinit var p : ProgressDialog
    lateinit var adb : AlertDialog.Builder

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_skrining_face_id,container,false)

        mediaHelper = MediaHelper(thisParent)
        v.btnFaceIdentification.setOnClickListener(this)
        v.btnOpenScrWeb.setOnClickListener(this)
        v.txSubjectId.setText("-")




        return v
    }

    override fun onClick(vw: View?) {
        when(vw?.id){
            R.id.btnFaceIdentification->{
                fileUri = mediaHelper.getOutputMediaFileUri()
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
                startActivityForResult(intent,mediaHelper.getRcCamera())
            }
            R.id.btnOpenScrWeb->{
                if(!v.txSubjectId.text.toString().equals("-")){
                    v.txSubjectId.setText("-")
                    v.txConfidence.setText("-")
                    v.txStatus.setText("-")
                    v.imv.setImageResource(R.drawable.ic_account_box_black_24dp)
                    thisParent.openWebViewScreening(true)
                }else{
                    thisParent.openWebViewScreening(false)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==mediaHelper.getRcCamera() && resultCode== Activity.RESULT_OK){
            imStr = mediaHelper.getBitmapToString(v.imv,fileUri)
            Toast.makeText(thisParent,data?.dataString,Toast.LENGTH_LONG).show()
            //Toast.makeText(thisParent,fileUri.toString(),Toast.LENGTH_LONG).show()
            searchFace(imStr,thisParent)
        }
    }

    fun searchFace(imStr : String, ctx : Context){
        val request = object : StringRequest(
            Method.POST,getString(R.string.url_recognize),
            Response.Listener {
                if(p.isShowing) p.dismiss()
                val jobj = JSONObject(it)
                try{
                    val images = jobj.getJSONArray("images")

                    val obj0 = images.getJSONObject(0)
                    val transaction = obj0.getJSONObject("transaction")
                    val status = transaction.getString("status")
                    if(!status.equals("failure")){
                        val subject_id = transaction.getString("subject_id")
                        val confidence = transaction.getDouble("confidence")
                        val persen = DecimalFormat("##.##%").format(confidence)
                        v.txSubjectId.setText(subject_id)
                        v.txConfidence.setText(persen)
                        v.txStatus.setText(status)

                        GlobalVariables.nikPasien = subject_id
                        GlobalVariables.fotoStr = imStr
                        GlobalVariables.fileUri = fileUri
                    }else{
                        v.txSubjectId.setText("-")
                        v.txConfidence.setText("-")
                        v.txStatus.setText("Wajah tidak dikenali")
                        adb = AlertDialog.Builder(thisParent)
                        adb.setTitle("Informasi")
                            .setMessage("Tidak ditemukan identitas yang sesuai di database," +
                                    "tambahkan wajah ke database?")
                            .setNegativeButton("Tidak",null)
                            .setPositiveButton("Ya",btnDialogYes)
                        adb.show()
                    }
                }catch (e : JSONException){
                    Toast.makeText(ctx,"Tidak ditemukan wajah", Toast.LENGTH_LONG).show()
                    v.txSubjectId.setText("-")
                    v.txConfidence.setText("-")
                    v.txStatus.setText("Tidak ditemukan wajah")
                }

            },
            Response.ErrorListener {
                if(p.isShowing) p.dismiss()
                Toast.makeText(ctx,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getHeaders(): MutableMap<String, String>{
                val hm = HashMap<String,String>()
                hm.put("app_id",getString(R.string.app_id))
                hm.put("app_key",getString(R.string.app_key))
                hm.put("Content-Type", "application/json");
                return hm
            }

            override fun getBody(): ByteArray {
                val jsonObject = JSONObject()
                jsonObject.put("gallery_name",getString(R.string.image_gallery))
                jsonObject.put("image",imStr)
                //jsonObject.put("threshold",0.7)
                //jsonObject.put("max_num_results",1)

                try{ return jsonObject.toString().toByteArray(Charsets.UTF_8)}
                catch (e: UnsupportedEncodingException){ return super.getBody()}
            }
        }

        p = ProgressDialog(thisParent)
        p.setTitle("Mohon ditunggu")
        p.setMessage("Sedang mencocokkan wajah....")
        p.setCancelable(false)
        p.show()

        val q = Volley.newRequestQueue(ctx)
        q.add(request)
    }

    fun enrollFace(nikPasien : String, adb : AlertDialog, ctx : Context){
        val request = object : StringRequest(
            Method.POST,getString(R.string.url_enroll),
            Response.Listener {
                if(p.isShowing) p.dismiss()
                GlobalVariables.nikPasien = nikPasien
                GlobalVariables.fotoStr = imStr
                GlobalVariables.fileUri = fileUri
                Toast.makeText(ctx,"Berhasil menambahkan wajah ke database", Toast.LENGTH_LONG).show()
                thisParent.openWebViewScreening(true)
                adb.dismiss()
            },
            Response.ErrorListener {
                if(p.isShowing) p.dismiss()
                Toast.makeText(ctx,"Tidak dapat terhubung ke server.\nGagal Menambahkan wajah", Toast.LENGTH_LONG).show()
            }){

            override fun getHeaders(): MutableMap<String, String>{
                val hm = HashMap<String,String>()
                hm.put("app_id",getString(R.string.app_id))
                hm.put("app_key",getString(R.string.app_key))
                hm.put("Content-Type", "application/json");
                return hm
            }

            override fun getBody(): ByteArray {
                val jsonObject = JSONObject()
                jsonObject.put("gallery_name",getString(R.string.image_gallery))
                jsonObject.put("subject_id",nikPasien)
                jsonObject.put("image",imStr)

                try{ return jsonObject.toString().toByteArray(Charsets.UTF_8)}
                catch (e:UnsupportedEncodingException){ return super.getBody()}
            }
        }

        p = ProgressDialog(thisParent)
        p.setTitle("Mohon ditunggu")
        p.setMessage("Sedang menambahkan wajah ke database ...")
        p.setCancelable(false)
        p.show()

        val q = Volley.newRequestQueue(ctx)
        q.add(request)
    }

    val btnDialogYes = DialogInterface.OnClickListener { _, _ ->
        val v = LayoutInflater.from(thisParent).inflate(R.layout.dialog_add_nik,null)
        val btnAdd = v.findViewById<Button>(R.id.btnDialogTambahNik)
        val btnCancel = v.findViewById<Button>(R.id.btnDialogBatalNik)
        val edNikPasien = v.findViewById<TextInputEditText>(R.id.tieDialogAddNik)

        val adb2 = AlertDialog.Builder(thisParent).create()
        adb2.setView(v)

        btnCancel.setOnClickListener { adb2.dismiss()}
        btnAdd.setOnClickListener { enrollFace(edNikPasien.text.toString(),adb2,thisParent) }

        adb2.show()
    }

}