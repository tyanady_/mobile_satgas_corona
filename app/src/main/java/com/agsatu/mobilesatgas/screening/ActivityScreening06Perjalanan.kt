package com.agsatu.mobilesatgas.screening

import android.content.Intent
import android.os.Bundle
import android.widget.SimpleAdapter
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import kotlinx.android.synthetic.main.activity_prosesreminder.*
import kotlinx.android.synthetic.main.activity_skrinning_perjalan.*

class ActivityScreening06Perjalanan : AppCompatActivity() {

    lateinit var adapterWargaDipantau : SimpleAdapter
    var arrWargaDipantau = ArrayList<HashMap<String,String>>()
    val arrWarga = arrayOf("Poniman","Poniran","Poniyem","Wagimin","Wagiman","Wagito","Warsidi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skrinning_perjalan)
        for(x in 0..arrWarga.size-1){
            val hm = HashMap<String,String>()
            hm.put("nama",arrWarga[x])
            arrWargaDipantau.add(hm)
        }
        adapterWargaDipantau = SimpleAdapter(this,arrWargaDipantau,R.layout.row_perjalanan,
            arrayOf("nama"), intArrayOf(R.id.tv_rute))
        ls_perjalanan.adapter = adapterWargaDipantau
    }

    override fun onBackPressed() {
        finish()
    }
}