package com.agsatu.mobilesatgas.screening

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import kotlinx.android.synthetic.main.activity_skrinning_fotoscreening.*

class ActivityScreening04KartuId : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skrinning_fotoscreening)
        btn_nextfotoscreen.setOnClickListener {
            startActivity(Intent(this,ActivityScreening05Kesehatan::class.java))
        }
    }

    override fun onBackPressed() {
        finish()
    }
}