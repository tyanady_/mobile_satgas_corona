package com.agsatu.mobilesatgas.screening

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import kotlinx.android.synthetic.main.activity_skrinning_ktp.*

class ActivityScreening01Ktp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skrinning_ktp)

        btn_nextktp.setOnClickListener {
            startActivity(Intent(this,ActivityScreening02Domisili::class.java))
        }
    }

    override fun onBackPressed() {
        finish()
    }


}