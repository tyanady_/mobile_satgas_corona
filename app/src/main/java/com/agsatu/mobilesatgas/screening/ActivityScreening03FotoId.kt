package com.agsatu.mobilesatgas.screening

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import kotlinx.android.synthetic.main.activity_skrinning_fotoidentitas.*

class ActivityScreening03FotoId : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skrinning_fotoidentitas)
        btn_nextfotoidentitas.setOnClickListener {
            startActivity(Intent(this,ActivityScreening04KartuId::class.java))
        }

    }

    override fun onBackPressed() {
        finish()
    }
}