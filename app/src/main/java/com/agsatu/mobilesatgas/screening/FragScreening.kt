package com.agsatu.mobilesatgas.screening

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.agsatu.mobilesatgas.main.MainActivity
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.login.LoginActivity
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.MediaHelper
import com.agsatu.mobilesatgas.utils.Utils
import kotlinx.android.synthetic.main.frag_skrinning.view.*
import org.json.JSONObject
import java.io.File
import java.io.FileNotFoundException
import java.net.URLEncoder


class FragScreening : Fragment() {
    lateinit var thisParent : MainActivity
    lateinit var v : View
    var js = ""
    var mFilePathCallback: ValueCallback<Array<Uri>>? = null
    var fileUri = Uri.parse("")
    lateinit var mediaHelper : MediaHelper
    lateinit var utils: Utils

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_skrinning,container,false)

        utils = Utils()
        mediaHelper = MediaHelper(thisParent)
        v.wvScreening.settings.domStorageEnabled=true
        v.wvScreening.settings.loadWithOverviewMode=true
        v.wvScreening.settings.allowFileAccess = true
        v.wvScreening.settings.javaScriptEnabled = true
        v.wvScreening.webChromeClient = PhotoWebChromeClient()

        if(GlobalVariables.nikPasien.equals("")) {
            v.wvScreening.webViewClient = WebViewClient()
            v.wvScreening.loadUrl(
                "${utils.URL_MOBILE}${GlobalVariables.nik}")
        }else{
            v.wvScreening.webViewClient = WebViewClient()
            val post = "nik=${GlobalVariables.nikPasien}&" +
                    "foto=${URLEncoder.encode(GlobalVariables.fotoStr,"UTF-8")}"
            v.wvScreening.postUrl(
                "${utils.URL_MOBILE_FACE}${GlobalVariables.nik}",
                //"http://192.168.43.32/covid19/upload2.php",
                post.toByteArray()
            )
            //v.edWebView.setText(post)
        }

        return v
    }

    inner class PhotoWebChromeClient : WebChromeClient(){
        override fun onShowFileChooser(
            webView: WebView?,
            filePathCallback: ValueCallback<Array<Uri>>?,
            fileChooserParams: FileChooserParams?
        ): Boolean {

            mFilePathCallback?.onReceiveValue(null)
            mFilePathCallback = filePathCallback

            fileUri = mediaHelper.getOutputMediaFileUri()
            val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            i.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
            val arrayIntent = Array(1){i}

            val itn = Intent(Intent.ACTION_GET_CONTENT);
            itn.addCategory(Intent.CATEGORY_OPENABLE);
            itn.setType("image/*");

            val mixItn = Intent(Intent.ACTION_CHOOSER)
            mixItn.putExtra(Intent.EXTRA_INTENT,itn)
            mixItn.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
            mixItn.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayIntent)
            mixItn.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
            startActivityForResult(Intent.createChooser(mixItn, "Pilih Gambar Dari : "),123);
            return true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode.equals(123)){
            if((data!= null) && (resultCode == Activity.RESULT_OK)){
                val dataString = data.dataString
                var results = Array(1){Uri.parse("")}
                if(dataString!=null){
                    val uriCompressedFile = mediaHelper.getOutputMediaFileUri()
                    mediaHelper.resizeBitmapFile(Uri.parse(dataString))
                    results[0] = uriCompressedFile
                }

                mFilePathCallback!!.onReceiveValue(results)
                mFilePathCallback = null
                return
            }

            else{
                try {
                    val uriCompressedFile = mediaHelper.getOutputMediaFileUri()
                    mediaHelper.resizeBitmapFile(fileUri)
                    val f = File(fileUri.path)
                    f.delete()

                    val results = Array(1){uriCompressedFile}
                    //Toast.makeText(thisParent,uriCompressedFile.toString(),Toast.LENGTH_LONG).show()
                    mFilePathCallback!!.onReceiveValue(results)
                    mFilePathCallback = null
                }catch (e:FileNotFoundException){
                    //Toast.makeText(thisParent,
                        //"RqC = $requestCode, RsC = $resultCode, ${e.localizedMessage.toString()}",
                        //Toast.LENGTH_LONG).show()
                    mFilePathCallback!!.onReceiveValue(null)
                    mFilePathCallback = null
                }

            }
        }
    }

}