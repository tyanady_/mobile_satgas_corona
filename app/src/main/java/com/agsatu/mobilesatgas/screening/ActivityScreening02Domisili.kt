package com.agsatu.mobilesatgas.screening

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import kotlinx.android.synthetic.main.activity_skrinning_domisili.*

class ActivityScreening02Domisili : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skrinning_domisili)
        btn_nextdomisili.setOnClickListener {
            startActivity(Intent(this,ActivityScreening03FotoId::class.java))
        }
    }

    override fun onBackPressed() {
        finish()
    }
}