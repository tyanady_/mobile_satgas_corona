package com.agsatu.mobilesatgas.main

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.reminder.ActivityReminderDetail
import com.agsatu.mobilesatgas.utils.Utils

class AdapterListReminder(val context : Context, arraylistM: ArrayList<HashMap<String,Any>>):
BaseAdapter(){

    val um : Utils
    var templistM : ArrayList<HashMap<String,Any>>

    init {
        listM = arraylistM
        templistM = ArrayList()
        um = Utils()
        templistM.addAll(AdapterListReminder.listM) }

    inner class ViewHolder(){

        var txNamaWarga : TextView? = null
        var txNikWarga : TextView? = null
        var txNoTelpWarga : TextView? = null
        var txStatusWarga : TextView? = null
        var txDesaWarga : TextView? = null
        var lsListWarga : ConstraintLayout? = null

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        var h = ViewHolder()
        if (view == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row_warga, null)
            h.txNamaWarga = view!!.findViewById(R.id.txNamaWargaDipantau) as TextView
            h.txNikWarga = view!!.findViewById(R.id.txNikWarga) as TextView
            h.txNoTelpWarga = view!!.findViewById(R.id.txNoTelpWarga) as TextView
            h.txStatusWarga = view!!.findViewById(R.id.txStatusWarga) as TextView
            h.txDesaWarga = view!!.findViewById(R.id.txDesaWarga) as TextView
            h.lsListWarga = view!!.findViewById(R.id.lsWarga) as ConstraintLayout



            view.tag = h

        } else {
            h = view.tag as ViewHolder  }

        var nama = listM[position].get("nama").toString()
        var nik = listM[position].get("nik").toString()
        var id_pasien = listM[position].get("id_pasien").toString()
        var no_hp = listM[position].get("no_hp").toString()
        var no_whatsapp = listM[position].get("no_whatsapp").toString()
        var kelurahan = listM[position].get("kelurahan").toString()
        var status_covid = listM[position].get("status_covid").toString()
        var tgl_kontrol = listM[position].get("tgl_kontrol").toString()
        var dom_kel = listM[position].get("dom_kel").toString()

        if (status_covid.equals("Negatif")){
            h.txStatusWarga!!.setBackgroundColor(Color.parseColor("#115c25"))
        }else if(status_covid.equals("ODP")){
            h.txStatusWarga!!.setBackgroundColor(Color.parseColor("#fff424"))
        }else if(status_covid.equals("PDP")){
            h.txStatusWarga!!.setBackgroundColor(Color.parseColor("#ba7600"))
        }else if(status_covid.equals("Confirm")){
            h.txStatusWarga!!.setBackgroundColor(Color.parseColor("#910000"))
        }else if(status_covid.equals("OTG")){
            h.txStatusWarga!!.setBackgroundColor(Color.parseColor("#290101"))
        }else{

        }


        h.lsListWarga!!.setOnClickListener{
            context.startActivity(Intent(context, ActivityReminderDetail::class.java).apply{
                putExtra("nama", nama )
                putExtra("nik", nik)
                putExtra("id_pasien", id_pasien)
                putExtra("no_hp", no_hp)
                putExtra("no_whatsapp", no_whatsapp)
                putExtra("kelurahan", kelurahan)
                putExtra("status_covid", status_covid)
                putExtra("tgl_kontrol", tgl_kontrol)
                putExtra("dom_kel", dom_kel)
            })
        }

        h.txNamaWarga!!.setText(listM[position].get("nama").toString())
        h.txNikWarga!!.setText(listM[position].get("nik").toString())
        h.txNoTelpWarga!!.setText(listM[position].get("no_hp").toString())
        h.txStatusWarga!!.setText(listM[position].get("status_covid").toString())
        h.txDesaWarga!!.setText(listM[position].get("kelurahan").toString())



        return view
    }

    override fun getItem(position: Int): Any {
        return listM.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listM.size
    }

    companion object{
        lateinit var listM : ArrayList<HashMap<String,Any>>}

}