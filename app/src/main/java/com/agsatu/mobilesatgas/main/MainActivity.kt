package com.agsatu.mobilesatgas.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.home.FragHome
import com.agsatu.mobilesatgas.screening.FragScreening
import com.agsatu.mobilesatgas.screening.FragScreeningFaceId
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.Utils
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.filter_sheet.*
import kotlinx.android.synthetic.main.frag_skrinning.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
class MainActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener  {
    var daftarStatus = mutableListOf<String>()
//    lateinit var statusAdapter : ArrayAdapter<String>

    lateinit var arrayList: ArrayList<HashMap<String,Any>>
    val um = Utils()

    var button_date: ImageButton? = null
    var button_date1: ImageButton? = null
    var cal = Calendar.getInstance()
    var tglawal: String=""
    var show: String=""
    var status: String=""

    var kec: String=""
    var kel: String=""
    var kab: String=""

    lateinit var AdapterListReminder : AdapterListReminder

    lateinit var ft : FragmentTransaction
    lateinit var fragHome : FragHome
    lateinit var fragScreen : FragScreening
    lateinit var fragScreeningFaceId: FragScreeningFaceId
    lateinit var clBtmSheet : LinearLayout
    lateinit var btmSheetBehavior: BottomSheetBehavior<LinearLayout>
    lateinit var rxPermissions : RxPermissions

//    val arrWarga = arrayOf("Poniman","Poniran","Poniyem","Wagimin","Wagiman","Wagito","Warsidi")

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//       statusAdapter = ArrayAdapter(
//            this, android.R.layout.simple_spinner_dropdown_item,
//            daftarStatus
//        )
//        spn_status.adapter = statusAdapter

        arrayList = ArrayList()

        fragHome = FragHome()
        fragScreen = FragScreening()
        fragScreeningFaceId = FragScreeningFaceId()

        clBtmSheet = findViewById(R.id.llBottomSheet)
        btmSheetBehavior = BottomSheetBehavior.from(clBtmSheet)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        btnFilter.setOnClickListener(this)
        btnReminderFilter.setOnClickListener(this)

        ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLayout,fragHome).commit()
        frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
        frameLayout.visibility = View.VISIBLE; setOnClickLlstener(this)
        tx_toolbar.setText("Home")

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);



// datapicker1
        button_date = this.btn_tnggalawal
        // create an OnDateSetListener
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker,  year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        }

        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        button_date!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@MainActivity, R.style.DialogTheme,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }
        })

// datapicker2
        button_date1 = this.btn_tanggalakhir
        // create an OnDateSetListener
        val dateSetListener1 = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker,  year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView1()
            }
        }
        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        button_date1!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@MainActivity, R.style.DialogTheme,
                    dateSetListener1,
                    // set DatePickerDialog to point to today's date when it loads up
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)).show()
            }
        })

        if(GlobalVariables.group.equals("SATGAS KECAMATAN")){
            kec = GlobalVariables.lokasi
//            Toast.makeText(this, kec, Toast.LENGTH_LONG).show()
        }else if(GlobalVariables.group.equals("SATGAS KELURAHAN")){
            kel = GlobalVariables.lokasi
//            Toast.makeText(this, kel, Toast.LENGTH_LONG).show()
        } else if(GlobalVariables.group.equals("SATGAS KABUPATEN")){
            kab = GlobalVariables.lokasi
//            Toast.makeText(this, kab, Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        getStatus()
    }

    private fun updateDateInView() {
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tx_tanggalawal.text = sdf.format(cal.getTime())
    }
    private fun updateDateInView1() {
        val myFormat = "yyyy-MM-dd" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tx_tanggalakhir.text = sdf.format(cal.getTime())
    }

    fun setOnClickLlstener(act : MainActivity){
        try {
            rxPermissions = RxPermissions(act)
            rxPermissions.request(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe()
            val m = StrictMode::class.java.
            getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemAbout -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragHome).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE
                tx_toolbar.setText("Home")
            }
            R.id.itemSkrinning ->{
                ft = supportFragmentManager.beginTransaction()
                //ft.replace(R.id.frameLayout,fragScreen).commit()
                ft.replace(R.id.frameLayout,fragScreeningFaceId).commit()
                frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
                frameLayout.visibility = View.VISIBLE
                tx_toolbar.setText("Screening\nIdentifikasi Wajah")

            }
            R.id.itemReminder ->{
                frameLayout.visibility = View.GONE
                tx_toolbar.setText("Reminder")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val current = LocalDateTime.now()
                    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
                    val tgl = current.format(formatter)
                    showReminder(tgl, "", "", "",kab, kec, kel)
                }
            }
            R.id.itemExit->{
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Konfirmasi").setMessage("Anda yakin ingin log out ?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton("Ya") { _, _ ->
                        val intent = Intent("clear_login_info")
                        sendBroadcast(intent)
                        finish()  }
                    .setNegativeButton("Tidak",null)
                    .setCancelable(false)
                builder.show()
            }
        }

        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnReminderFilter -> {
                btmSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            R.id.btnFilter ->{
                val tgl_awal = tx_tanggalawal.text.toString()
                val tgl_akhir = tx_tanggalakhir.text.toString()
                status = spn_status.selectedItem.toString()
                if (status.equals("Pilih Semua")){
                    status = ""
                }
                val nama = ed_namadanidentitas.text.toString()

                btmSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                showReminder(tgl_awal, tgl_akhir, status, nama, kab, kec, kel)
            }
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK)){
            if(btmSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED){
                btmSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }else{
                if(fragScreen.isVisible && fragScreen.v.wvScreening.canGoBack()){
                    fragScreen.v.wvScreening.goBack() }
            }
        }
        return false
    }


    fun getStatus(){
        val request = StringRequest(
            Request.Method.POST, um.URL_DROPDOWN_STATUS,
            Response.Listener {
                    response ->
                daftarStatus.clear()
                val j = JSONObject(response)
                val araydata = j.getJSONArray("data")
                for (x in 0..(araydata.length()-1)) {
                    val jsonOb1 = araydata.getJSONObject(x)
                    val status = jsonOb1.getString("singkatan")
                    daftarStatus.add(status)

                }
//                statusAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                    _ ->
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }




    fun showReminder(tgl_awal: String, tgl_akhir: String, status: String, nama: String, kab: String, kec: String, kel: String){
        val request = object : StringRequest(
            Method.POST, um.URL_SHOW_REMINDER, Response.Listener{ response ->
                arrayList.clear()
                try {
                    val j = JSONObject(response)
                    val araydata = j.getJSONArray("data")
                    for (x in 0..(araydata.length()-1)) {
                        val jsonOb1 = araydata.getJSONObject(x)
//                        val status = jsonOb1.getString("status")
//                        if (!status.equals("1")) {
//                            Toast.makeText(this, "Data tidak Ditemukan", Toast.LENGTH_LONG).show()
//                        } else {

                        val nama = jsonOb1.getString("nama")
                        val nik = jsonOb1.getString("nik")
                        val id_pasien = jsonOb1.getString("id_pasien")
                        val no_hp = jsonOb1.getString("no_hp")
                        val no_whatsapp = jsonOb1.getString("no_whatsapp")
                        val status_covid = jsonOb1.getString("status_covid")
                        val kelurahan = jsonOb1.getString("kelurahan")
                        val tgl_kontrol = jsonOb1.getString("tgl_kontrol")
                        val dom_kel = jsonOb1.getString("dom_kel")


                        val hm = java.util.HashMap<String, Any>()
                        hm.put("nama", nama)
                        hm.put("nik", nik)
                        hm.put("id_pasien", id_pasien)
                        hm.put("no_hp", no_hp)
                        hm.put("no_whatsapp", no_whatsapp)
                        hm.put("status_covid", status_covid)
                        hm.put("kelurahan", kelurahan)
                        hm.put("tgl_kontrol", tgl_kontrol)
                        hm.put("dom_kel", dom_kel)


                        arrayList.add(hm)
//                        }

                        AdapterListReminder = AdapterListReminder(this, arrayList)
                        lsWargaReminder.adapter = AdapterListReminder
                    }

                } catch (e: Exception) {
                    arrayList.clear()
                    val hm = java.util.HashMap<String, Any>()
                    hm.put("nama", "")
                    hm.put("nik", "")
                    hm.put("no_hp", "")
                    hm.put("no_whatsapp", "")
                    hm.put("status_covid","")
                    hm.put("kelurahan", "")
                    hm.put("tgl_kontrol", "")
                    hm.put("dom_kel", "")


                    arrayList.add(hm)
                    AdapterListReminder = AdapterListReminder(this, arrayList)
                    lsWargaReminder.adapter = AdapterListReminder

                    Toast.makeText(this, "Data Tidak Ditemukan", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{ _ ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hmm = java.util.HashMap<String, String>()
                hmm.put("tgl_awal", tgl_awal)
                hmm.put("tgl_akhir", tgl_akhir)
                hmm.put("status", status)
                hmm.put("nama", nama)
                hmm.put("kec", kec)
                hmm.put("kel", kel)
                hmm.put("kab", kab)
                return hmm
            }

        }
        val queue1 = Volley.newRequestQueue(this)
        queue1.add(request)
    }


    fun openWebViewScreening(hasData : Boolean){
        if(hasData == false){
            GlobalVariables.fotoStr=""
            GlobalVariables.nikPasien=""
            GlobalVariables.fileUri=Uri.parse("")
        }

        ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLayout,fragScreen).commit()
        frameLayout.setBackgroundColor(Color.argb(255,255,255,255))
        frameLayout.visibility = View.VISIBLE
        tx_toolbar.setText("Screening")
    }

    //    fun showlistReminder(){
//        val request = object : StringRequest(
//            Method.POST, um.URL_SHOW_REMINDER, Response.Listener{ response ->
//                arrayList.clear()
//                val j = JSONObject(response)
//                val status = j.getString("code")
//                if(status.equals("100")) {
//                try {
//
//                    val araydata = j.getJSONArray("data")
//                    for (x in 0..(araydata.length()-1)) {
//                        val jsonOb1 = araydata.getJSONObject(x)
////                        val status = jsonOb1.getString("status")
////                        if (!status.equals("1")) {
////                            Toast.makeText(this, "Data tidak Ditemukan", Toast.LENGTH_LONG).show()
////                        } else {
//                            val nama = jsonOb1.getString("nama")
//                            val nik = jsonOb1.getString("nik")
//                            val no_hp = jsonOb1.getString("no_hp")
//                            val no_whatsapp = jsonOb1.getString("no_whatsapp")
//                            val status_covid = jsonOb1.getString("status_covid")
//                            val kelurahan = jsonOb1.getString("kelurahan")
//                            val tgl_kontrol = jsonOb1.getString("tgl_kontrol")
//                            val dom_kel = jsonOb1.getString("dom_kel")
//
//
//                            val hm = java.util.HashMap<String, Any>()
//                            hm.put("nama", nama)
//                            hm.put("nik", nik)
//                            hm.put("no_hp", no_hp)
//                            hm.put("no_whatsapp", no_whatsapp)
//                            hm.put("status_covid", status_covid)
//                            hm.put("kelurahan", kelurahan)
//                            hm.put("tgl_kontrol", tgl_kontrol)
//                            hm.put("dom_kel", dom_kel)
//
//
//                            arrayList.add(hm)
////                        }
//
//                        AdapterListReminder = AdapterListReminder(this, arrayList)
//                        lsWargaReminder.adapter = AdapterListReminder
//                    }
//
//                } catch (e: Exception) {
//                    arrayList.clear()
//                    val hm = java.util.HashMap<String, Any>()
//                    hm.put("nama", "")
//                    hm.put("nik", "")
//                    hm.put("no_hp", "")
//                    hm.put("no_whatsapp", "")
//                    hm.put("status_covid","")
//                    hm.put("kelurahan", "")
//                    hm.put("tgl_kontrol", "")
//                    hm.put("dom_kel", "")
//
//
//                    arrayList.add(hm)
//                    AdapterListReminder = AdapterListReminder(this, arrayList)
//                    lsWargaReminder.adapter = AdapterListReminder
//
//                    Toast.makeText(this, "Data Tidak Ditemukan", Toast.LENGTH_LONG).show()
//                }
//                }else{
//                    Toast.makeText(this, "Data Tidak Ditemukan", Toast.LENGTH_LONG).show()
//                }
//
//            },
//            Response.ErrorListener{ error ->
//                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
//            }){
//
//        }
//        val queue1 = Volley.newRequestQueue(this)
//        queue1.add(request)
//    }

//    override fun onBackPressed() {
//        if(btmSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED){
//            btmSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
//        }else{
//            if(fragScreen.isVisible && fragScreen.v.wvScreening.canGoBack()){
//                fragScreen.v.wvScreening.goBack()
//            }else{
//                super.onBackPressed()
//            }
//        }
//    }


}

