package com.agsatu.mobilesatgas.reminder

import android.Manifest.permission.CALL_PHONE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.main.MainActivity
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.Utils
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_detailreminder.*
import kotlinx.android.synthetic.main.wa_sheet.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class ActivityReminderDetail : AppCompatActivity(), View.OnClickListener{

    lateinit var judulAdapter : ArrayAdapter<String>
    var daftarJudul = mutableListOf<String>()
    var template_id : String =""
    var template_isi : String =""

    lateinit var araydata : JSONArray

    val um = Utils()
    var nama : String =""
    var no_hp : String=""
    var no_whatsapp : String=""
    var tgl_kontrol : String=""
    var nik : String=""
    var id_pasien : String=""

 //   val CompanyWeb = "https://api.whatsapp.com/send?phone=6285704079747&text=Saya%20tertarik%20dengan%20mobil%20Anda%20yang%20dijual&source=&data=&app_absent="


    var id_reminder :String=""
    var aktivitas_reminder : String=""
    var hasil_reminder : String=""
    var status_kooperatif : String=""
    var created_by : String=""
    var created_at : String=""

    lateinit var clWaSheet : LinearLayout
    lateinit var waSheetBehavior: BottomSheetBehavior<LinearLayout>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailreminder)

        getJudul()
        judulAdapter = ArrayAdapter(
            this, android.R.layout.simple_spinner_dropdown_item,
            daftarJudul
        )
        spn_wa.adapter = judulAdapter
        spn_wa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                template_id = ""
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                template_id  = getIsi(position)
//                btnKirimWa.setText(template_id)
                getTemplate(id_pasien,template_id)
            }

        }


        ed_idtiket1.visibility = View.GONE
        ed_idtiket2.visibility = View.GONE
        image3.visibility = View.GONE
        image4.visibility = View.GONE
        textView11.visibility = View.GONE
        textView32.visibility = View.GONE

//
        clWaSheet = findViewById(R.id.llWaSheet)
        waSheetBehavior = BottomSheetBehavior.from(clWaSheet)

        val data = intent
        nama = data.getStringExtra("nama")
        nik = data.getStringExtra("nik")
        id_pasien = data.getStringExtra("id_pasien")
        val dom_kel = data.getStringExtra("dom_kel")
        tgl_kontrol = data.getStringExtra("tgl_kontrol")
        no_hp = data.getStringExtra("no_hp")
        no_whatsapp = data.getStringExtra("no_whatsapp")
        //val kelurahan = data.getStringExtra("kelurahan")
        //val status_covid = data.getStringExtra("status_covid")

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val formatter =  DateTimeFormatter.ofPattern("dd MMMM yyyy")
//            val tanggal_mulai = tgl_kontrol.format(formatter)
//            tx_tglmulaireminder.setText(tanggal_mulai)
//        }
//        else {
//            val pattern = "dd MMMM yyyy"
//            val simpleDateFormat = SimpleDateFormat(pattern)
//            val tanggal_mulai = simpleDateFormat.format(tgl_kontrol)
//            tx_tglmulaireminder.setText(tanggal_mulai)
//        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val now = LocalDateTime.now()
            val format = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss")
            val tanggal = now.format(format)
            tx_norimender.setText("RMD_"+dom_kel+tanggal)
        }

        tx_tglmulaireminder.setText(tgl_kontrol)
        tx_namareminder.setText(nama)
        tx_nikreminder.setText(nik)
        ed_namapetugas.setText(GlobalVariables.nama)
        ed_jabatan.setText(GlobalVariables.profesi)


        button2.setOnClickListener(this)
        btn_remindermanual.setOnClickListener(this)
        btn_reminderhistory.setOnClickListener(this)
        btn_remindersistem.setOnClickListener(this)
        btn_simpanreminder.setOnClickListener(this)
        btn_wareminder.setOnClickListener(this)
        btn_smsreminder.setOnClickListener(this)
        btn_closereminder.setOnClickListener(this)
        btnKirimWa.setOnClickListener(this)

    }

    override fun onBackPressed() {
        finish()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.btn_wareminder -> {
//                var substring_nowa = no_whatsapp.subSequence(1, no_whatsapp.length)
//                val CompanyWeb = "https://api.whatsapp.com/send?phone=62"+substring_nowa+"&text=Saya%20tertarik%20dengan%20mobil%20Anda%20yang%20dijual&source=&data=&app_absent="
//                var url: String = CompanyWeb.trim()
//                if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://$url"
//                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
//                startActivity(browserIntent)
//                browserIntent.setPackage("com.whatsapp")
//                if (browserIntent.resolveActivity(packageManager) != null) {
//                    startActivity(browserIntent)
//                } else {
//                    Toast.makeText(
//                        this@ActivityReminderDetail,
//                        "whatsapp not install please install whatsapp",
//                        Toast.LENGTH_LONG
//                    ).show()
//                }
                waSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            R.id.btn_smsreminder -> {
                val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + no_hp))

                if (ActivityCompat.checkSelfPermission(this@ActivityReminderDetail, CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent)
            }
            R.id.btn_closereminder -> {
                val uri = Uri.parse("smsto:"+ no_hp)
                val intent = Intent(Intent.ACTION_SENDTO, uri)
                intent.putExtra("sms_body", "Here goes your message...")
                startActivity(intent)
            }
            R.id.btn_remindersistem -> {
                textView10.setText("Reminder Sistem")
                scrollView2.visibility =  View.GONE
            }
            R.id.btn_remindermanual -> {
                textView10.setText("Reminder Manual")
                scrollView2.visibility =  View.VISIBLE
            }
            R.id.btn_reminderhistory -> {
                startActivity(Intent(this,ActivityProsesReminder::class.java)
                    .apply{
                        putExtra("nik", nik)

                    })
            }
            R.id.button2 -> {
                startActivity(Intent(this,MainActivity::class.java))
            }
            R.id.btnKirimWa -> {
                var substring_nowa = no_whatsapp.subSequence(1, no_whatsapp.length)
                val CompanyWeb = "https://api.whatsapp.com/send?phone=62"+substring_nowa+"&text="+template_isi+"&source=&data=&app_absent="
                var url: String = CompanyWeb.trim()
                if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://$url"
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(browserIntent)
                browserIntent.setPackage("com.whatsapp")
                if (browserIntent.resolveActivity(packageManager) != null) {
                    startActivity(browserIntent)
                } else {
                    Toast.makeText(
                        this@ActivityReminderDetail,
                        "whatsapp not install please install whatsapp",
                        Toast.LENGTH_LONG
                    ).show()
                }
                waSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.btn_simpanreminder -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val now = LocalDateTime.now()
                    val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                    created_at = now.format(format)

                }

                id_reminder = tx_norimender.text.toString()
                aktivitas_reminder = ed_aktivitas.text.toString()
                hasil_reminder = ed_hasil.text.toString()
                status_kooperatif = spn_kooperatif.selectedItem.toString()
                created_by = GlobalVariables.id_personil


                InsertReminder(nik,id_reminder, aktivitas_reminder, hasil_reminder, status_kooperatif, created_by, created_at)
            }
        }

    }
    fun InsertReminder(id_pasien: String, id_reminder: String, aktivitas_reminder: String, hasil_reminder: String, status_kooperatif: String, created_by: String, created_at: String){
        val request = object : StringRequest(
            Method.POST, um.URL_INSERT_REMINDER,
            Response.Listener{
                val jsonOb = JSONObject(it)
//                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                val status = jsonOb.getString("code")
                if(status.equals("500")){
                    Toast.makeText(this, "Data Berhasil diinput", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, "Data Gagal diinput", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{ _ ->

                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hmm = HashMap<String, String>()
                hmm.put("id_pasien", id_pasien)
                hmm.put("id_reminder", id_reminder)
                hmm.put("aktivitas_reminder", aktivitas_reminder)
                hmm.put("hasil_reminder", hasil_reminder)
                hmm.put("status_kooperatif", status_kooperatif)
                hmm.put("created_by", created_by)
                hmm.put("created_at", created_at)

                return hmm
           }
        }
        val queue1 = Volley.newRequestQueue(this)
        queue1.add(request)
    }

    fun getJudul(){
        val request = StringRequest(
            Request.Method.POST, um.URL_DROPDOWN_WA,
            Response.Listener {
                    response ->
                daftarJudul.clear()
                val j = JSONObject(response)
                araydata = j.getJSONArray("data")
                for (x in 0..(araydata.length()-1)) {
                    val jsonOb1 = araydata.getJSONObject(x)
                    val judul = jsonOb1.getString("nama_template")
                    daftarJudul.add(judul)
                }

                judulAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                    _ ->
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun getIsi(position: Int): String {
        var id_template : String = ""
        try {
            val jsonObj = araydata.getJSONObject(position)
            id_template = jsonObj.getString("id_template")

        }catch (e: Exception){

        }

        return id_template
    }



    fun getTemplate(id_pasien: String, id_template: String){
        val request = object : StringRequest(
            Method.POST, um.URL_TEMPLATE_WA,
            Response.Listener{
                val jsonOb = JSONObject(it)
                template_isi = jsonOb.getString("data")

//                    Toast.makeText(this, template_isi, Toast.LENGTH_LONG).show()
            },
            Response.ErrorListener{ _ ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hmm = HashMap<String, String>()
                hmm.put("id_template", id_template)
                hmm.put("id_pasien", id_pasien)
                return hmm
            }
        }
        val queue1 = Volley.newRequestQueue(this)
        queue1.add(request)
    }


}