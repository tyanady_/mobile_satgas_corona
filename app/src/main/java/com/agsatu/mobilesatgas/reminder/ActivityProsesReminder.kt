package com.agsatu.mobilesatgas.reminder

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.main.AdapterListReminder
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.Utils
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_prosesreminder.*
import org.json.JSONArray
import org.json.JSONObject

class ActivityProsesReminder : AppCompatActivity(), View.OnClickListener {

    lateinit var arrayList: ArrayList<HashMap<String,Any>>
    val um = Utils()

    var id_pasien = ""
    var created_by = ""
    lateinit var AdapterHistoryReminder : AdapterHistoryReminder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prosesreminder)

        arrayList = ArrayList()

        val data = intent
        id_pasien = data.getStringExtra("nik")
        created_by = GlobalVariables.id_personil

        showlistProsesReminder(id_pasien,created_by)

        btn_backhistory.setOnClickListener(this)



    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_backhistory -> {
                finish()
            }
        }
    }
    fun showlistProsesReminder(id_pasien: String, created_by: String){
        val request = object : StringRequest(
            Method.POST, um.URL_HISTORY_REMINDER, Response.Listener{ response ->
                arrayList.clear()
                val json1 = JSONObject(response)
                val status = json1.getString("code")
                if(status.equals("100")) {

                try {
                        val araydata = json1.getJSONArray("data")
                        for (x in 0..(araydata.length() - 1)) {
                            val jsonOb1 = araydata.getJSONObject(x)

                            val id_reminder = jsonOb1.getString("id_reminder")
                            val created_at = jsonOb1.getString("created_at")

                            val hm = java.util.HashMap<String, Any>()
                            hm.put("id_reminder", id_reminder)
                            hm.put("created_at", created_at)

                            arrayList.add(hm)
//                        }

                            AdapterHistoryReminder = AdapterHistoryReminder(this, arrayList)
                            ls_riwayatwarga.adapter = AdapterHistoryReminder
                        }
                } catch (e: Exception) {
                    arrayList.clear()
                    val hm = java.util.HashMap<String, Any>()
                    hm.put("id_reminder", "")
                    hm.put("created_at", "")

                    arrayList.add(hm)
                    AdapterHistoryReminder = AdapterHistoryReminder(this, arrayList)
                    lsWargaReminder.adapter = AdapterHistoryReminder

                    Toast.makeText(this, "Data Tidak Ditemukan", Toast.LENGTH_LONG).show()
                }

                }else{
                    Toast.makeText(this, "Data Tidak Ditemukan", Toast.LENGTH_LONG).show()
                }

            },
            Response.ErrorListener{ _ ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hmm = java.util.HashMap<String, String>()
                hmm.put("id_pasien", id_pasien)
                hmm.put("created_by", created_by)

                return hmm
            }
        }
        val queue1 = Volley.newRequestQueue(this)
        queue1.add(request)
    }

    override fun onBackPressed() {
        finish()
    }

}