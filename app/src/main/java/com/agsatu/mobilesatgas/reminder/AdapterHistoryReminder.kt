package com.agsatu.mobilesatgas.reminder

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.utils.Utils
import kotlinx.android.synthetic.main.activity_detailreminder.*
import java.time.format.DateTimeFormatter
import kotlin.math.tan

class AdapterHistoryReminder(val context : Context, arraylistM: ArrayList<HashMap<String,Any>>):
    BaseAdapter(){

    val um : Utils
    var templistM : ArrayList<HashMap<String,Any>>

    init {
        listM = arraylistM
        templistM = ArrayList()
        um = Utils()
        templistM.addAll(AdapterHistoryReminder.listM) }

    inner class ViewHolder(){

        var txIdReminder : TextView? = null
        var txTanggalReminder : TextView? = null

    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        var h = ViewHolder()
        if (view == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row_prosesreminder, null)
            h.txIdReminder = view!!.findViewById(R.id.tv_namapasien) as TextView
            h.txTanggalReminder= view!!.findViewById(R.id.tx_tanggalhistory) as TextView

            view.tag = h

        } else {
            h = view.tag as ViewHolder  }

        //var id_reminder = listM[position].get("id_reminder").toString()
        //var created_at = listM[position].get("created_at").toString()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //val formatter =  DateTimeFormatter.ofPattern("dd MMMM yyyy")
            //val tanggal= created_at.format(formatter)

        }

        h.txIdReminder!!.setText(listM[position].get("id_reminder").toString())
        h.txTanggalReminder!!.setText(listM[position].get("created_at").toString())





        return view
    }

    override fun getItem(position: Int): Any {
        return listM.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listM.size
    }

    companion object{
        lateinit var listM : ArrayList<HashMap<String,Any>>}

}