package com.agsatu.mobilesatgas.login

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.agsatu.mobilesatgas.R
import com.agsatu.mobilesatgas.main.MainActivity
import com.agsatu.mobilesatgas.utils.GlobalVariables
import com.agsatu.mobilesatgas.utils.Utils
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.util.*


class LoginActivity : AppCompatActivity() {
    val um = Utils()
    var username : String=""
    var password : String=""
    var kec: String=""
    var kel: String=""
    var kab: String=""

    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action
            if(action.equals("clear_login_info")) {
                edPassword.setText("")
                edUsername.setText("")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        registerReceiver(broadcastReceiver, IntentFilter("clear_login_info"))

        btn_login.setOnClickListener {
            username = edUsername.text.toString()
            password = edPassword.text.toString()
            LoginReminder(username,password)
//            val launchIntent = packageManager.getLaunchIntentForPackage("com.google.android.you")
//            if (launchIntent != null) {
//                startActivity(launchIntent)
//            } else {
//                Toast.makeText(this, "Aplikasi Belum di Install", Toast.LENGTH_LONG).show()
//            }
        }

        //ShowPassword
        show.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, value ->
            if (value) { // Show Password
                edPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            } else { // Hide Password
                edPassword.setTransformationMethod(PasswordTransformationMethod.getInstance())
            }
        })


    }

    fun LoginReminder(username: String, password: String){
        val request = object : StringRequest(
            Method.POST, um.URL_LOGIN_REMINDER,
            Response.Listener{
                val jsonOb = JSONObject(it)
//                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                val status = jsonOb.getString("code")
                if(status.equals("502")){
                    val araydata = jsonOb.getJSONArray("data")
                    val jsonOb1 = araydata.getJSONObject(0)
                    GlobalVariables.nama  = jsonOb1.getString("nama")
                    GlobalVariables.profesi = jsonOb1.getString("profesi")
                    GlobalVariables.id_personil = jsonOb1.getString("id")
                    GlobalVariables.nik = jsonOb1.getString("nik")
                    GlobalVariables.lokasi = jsonOb1.getString("lokasi")
                    GlobalVariables.group = jsonOb1.getString("group")

                    Toast.makeText(this, "Login Sukses", Toast.LENGTH_LONG).show()
//                    Toast.makeText(this, GlobalVariables.group, Toast.LENGTH_LONG).show()
                    startActivity(Intent(this, MainActivity::class.java))
                }else{
                    Toast.makeText(this, "Login Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener{ _ ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hmm = HashMap<String, String>()
                hmm.put("username", username)
                hmm.put("password", password)
                return hmm
            }
        }
        val queue1 = Volley.newRequestQueue(this)
        queue1.add(request)
    }

    override fun onBackPressed() {
        finish()
    }


}