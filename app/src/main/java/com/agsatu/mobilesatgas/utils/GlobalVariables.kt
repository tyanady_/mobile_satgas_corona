package com.agsatu.mobilesatgas.utils

import android.app.Application
import android.net.Uri

class GlobalVariables : Application() {
    companion object{
        var nama = ""
        var profesi =""
        var id_personil= ""
        var nik =""
        var lokasi =""
        var group =""

        var nikPasien =""
        var fotoStr = ""
        var fileUri : Uri = Uri.parse("")
    }
}