package com.agsatu.mobilesatgas.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

@Suppress("DEPRECATION")
class MediaHelper(val ctx : Context) {
    var namaFile=""
    var fileUri = Uri.parse("")

    fun getRcCamera(): Int{
        return 1223
    }

    fun bitmapToString(bmp : Bitmap, uri: Uri) : String{
        val outputStream = ByteArrayOutputStream()
        val fileOutputStream = FileOutputStream(uri.toString().replace("file://",""))

        bmp.compress(Bitmap.CompressFormat.JPEG,60,outputStream)
        bmp.compress(Bitmap.CompressFormat.JPEG,60,fileOutputStream)

        val byteArray = outputStream.toByteArray()
        val str = Base64.encodeToString(byteArray, Base64.DEFAULT)

        outputStream.close()
        fileOutputStream.close()

        return str
    }

    fun getBitmapToString(imV : ImageView, uri : Uri) : String{
        var bmp = BitmapFactory.decodeFile(this.fileUri.path)
        var dim = 1280
        if(bmp.height > bmp.width){
            bmp = Bitmap.createScaledBitmap(bmp,
                (bmp.width*dim).div(bmp.height),dim,true)
        }else{
            //rotasi bitmap 90 derajat. biasanya default sensor landscape
            //rotasi 90 derajat
            val camera = Camera.CameraInfo()
            Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK,camera)

            val mtx = Matrix()
            mtx.postRotate(camera.orientation.toFloat() )

            bmp = Bitmap.createScaledBitmap(bmp,
                dim, (bmp.height*dim).div(bmp.width),true)
            //bmp = Bitmap.createBitmap(bmp,0,0,bmp.width,bmp.height,mtx,true)

        }
        imV.setImageBitmap(bmp)
        return  bitmapToString(bmp, uri)
    }


    fun getOutputMediaFile() : File?{
        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DCIM),"APP_COVID_19")
        if(!mediaStorageDir.exists())
            if(!mediaStorageDir.mkdirs()){
                Log.e("mkdir","Gagal membuat direktori")
            }
        val mediaFile = File(mediaStorageDir.path+File.separator+
                "${this.namaFile}")
        return mediaFile
    }

    fun getOutputMediaFileUri(): Uri {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())
        this.namaFile = "DC_${timeStamp}.jpg"
        this.fileUri = Uri.fromFile(getOutputMediaFile())
        return this.fileUri
    }

    fun resizeBitmapFile(uri:Uri){
        var bmp = MediaStore.Images.Media.getBitmap(ctx.contentResolver, uri)

        var dim = 1024
        if(bmp.height > bmp.width){
            bmp = Bitmap.createScaledBitmap(bmp,
                (bmp.width*dim).div(bmp.height),dim,true)
        }else{
            bmp = Bitmap.createScaledBitmap(bmp,
                dim, (bmp.height*dim).div(bmp.width),true)
        }
        val fileOutputStream = FileOutputStream(this.fileUri.toString().replace("file://",""))
        bmp.compress(Bitmap.CompressFormat.JPEG,60,fileOutputStream)
        fileOutputStream.close()
    }
}