package com.agsatu.mobilesatgas.utils

import android.util.Base64
import java.security.SecureRandom
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class Utils {
    var dfs : DecimalFormatSymbols
    var df : DecimalFormat

    val random = SecureRandom()
    var buffer = ByteArray(20)
    var secureUid = ""

    val URL_ROOT = "https://demo.smartcovid19.com/api"

    val URL_SHOW_REMINDER   = "$URL_ROOT/list_reminder"
    val URL_INSERT_REMINDER   = "$URL_ROOT/insert_reminder"
    val URL_HISTORY_REMINDER   = "$URL_ROOT/history_reminder"
    val URL_LOGIN_REMINDER   = "$URL_ROOT/login_reminder"
    val URL_FILTER_REMINDER   = "$URL_ROOT/filter_list_reminder"
    val URL_DROPDOWN_STATUS  = "$URL_ROOT/dropdown_status"
    val URL_DROPDOWN_WA = "$URL_ROOT/get_all_template"
    val URL_TEMPLATE_WA = "$URL_ROOT/respon_template"
    val URL_MOBILE = "http://demo.smartcovid19.com/screeningmobile?mobile=1&personil_user="
    val URL_MOBILE_FACE = "http://demo.smartcovid19.com/screeningmobile/create_from_fid?mobile=1&personil_user="

    init {
        dfs = DecimalFormatSymbols()
        dfs.groupingSeparator = '.'
        dfs.decimalSeparator = ','
        dfs.currencySymbol = "Rp"
        df = DecimalFormat("\u00A4 ###,###",dfs)
    }

    fun formatRp(value : Double):String{
        return df.format(value)
    }

    fun formatRp(value : Int):String{
        return df.format(value)
    }

    fun generateRandomUid() : String{
        random.nextBytes(buffer)
        secureUid = Base64.encodeToString(buffer, Base64.URL_SAFE)
        return secureUid.take(20)
    }
}